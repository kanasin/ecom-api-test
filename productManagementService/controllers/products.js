const axios = require('axios')
const products = require('../models/products')

module.exports = function (pool) {
  return {
    async getProducts(ctx, next){
      try{
        const result = await products.getProducts(pool)
        ctx.body = result
      }catch(e){
        ctx.response.status = 400
        ctx.body = e.message
      }finally{
        next()
      }
    },
    async getProduct(ctx, next){
      try{
        const { id } = ctx.params
        const result = await products.getProduct(pool, parseInt(id))
        ctx.body = result
      }catch(e){
        ctx.response.status = 400
        ctx.body = e.message
      }finally{
        next()
      }
    },
  }
}
