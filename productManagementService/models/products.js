module.exports = {
  async getProducts(pool){
    const query = `select * from products`
    const [rows] = await pool.query(query)
    return rows
  },
  async getProduct(pool, id){
    const query = `select * from products where id = ?`
    const [rows] = await pool.query(query, [id])
    return rows[0]
  }
}
