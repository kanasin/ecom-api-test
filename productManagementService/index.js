const Koa = require('koa');
const app = new Koa();
const bodyParser = require("koa-bodyparser");
const Router = require('koa-router')
const router = new Router()
const bearerToken = require('koa-bearer-token')
const mysql = require('mysql2/promise')

const port = 3400

const products = require('./controllers/products')

const config = require('./config/config.json')

const pool = mysql.createPool({
  user: config.db.user, 
  password: config.db.password, 
  database: config.db.db, 
  host: config.db.host,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0
})

const productsManage = products(pool)

router.get("/", productsManage.getProducts)
router.get("/:id", productsManage.getProduct)

app.use(bodyParser())
app.use(bearerToken())
app.use(router.routes())

console.log(`Server listening on ${port}`);
app.listen(port);