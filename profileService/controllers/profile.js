const axios = require('axios')
const profile = require('../models/profile')

module.exports = function (pool) {
  return {
    async getProfile(ctx, next){
      try{
        const authen = await axios.get(`http://localhost:3100/`, 
          {
            headers: {'Authorization': `Bearer ${ctx.request.token}`}
          })
        if(authen.data.id){
          ctx.body = authen.data
        }else{
          ctx.response.status = 400
          ctx.body = 'authen fail'
        }
      }catch(e){
        ctx.response.status = 400
        ctx.body = e.message
      }finally{
        next()
      }
    }
  }
}
