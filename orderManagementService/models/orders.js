module.exports = {
  async getOrders(pool, userId){
    const query = `select * from orders where users_id = ?`
    const [rows] = await pool.query(query, [ userId ])
    return rows
  },
  async addOrder(pool, data){
    const query = `insert into orders (users_id, product_id, product_name, price) values (?, ?, ?, ?)`
    const [rows] = await pool.query(query, [ data.users_id, data.product_id, data.product_name, data.price ])
    return rows
  },
  async removeOrder(pool, data){
    const query = `delete from orders where id = ?`
    const [rows] = await pool.query(query, [ data ])
    return rows
  }
}
