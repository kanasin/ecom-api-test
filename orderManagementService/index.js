const Koa = require('koa');
const app = new Koa();
const bodyParser = require("koa-bodyparser");
const Router = require('koa-router')
const router = new Router()
const axios = require('axios')
const bearerToken = require('koa-bearer-token')
const mysql = require('mysql2/promise')

const orders = require('./controllers/orders')

const config = require('./config/config.json')

const port = 3500

const pool = mysql.createPool({
  user: config.db.user, 
  password: config.db.password, 
  database: config.db.db, 
  host: config.db.host,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0
})

const ordersManage = orders(pool)

router.get("/", ordersManage.getOrders)

router.post("/addOrder", ordersManage.addOrders)

router.delete("/:id", ordersManage.removeOrder)

app.use(bodyParser());
app.use(bearerToken())
app.use(router.routes())

console.log(`Server listening on ${port}`);
app.listen(port);