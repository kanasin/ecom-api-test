const axios = require('axios')
const config = require('./../config/config.json')
const orders = require('../models/orders')

module.exports = function (pool) {
  return {
    async getOrders(ctx, next){
      try{
        const authen = await axios.get(`http://localhost:3100/`, 
          {
            headers: {'Authorization': `Bearer ${ctx.request.token}`}
          })
        if(authen.data.id){
          const result = await orders.getOrders(pool, authen.data.id)
          ctx.body = result
        }else{
          ctx.response.status = 400
          ctx.body = 'authen fail'
        }
      }catch(e){
        ctx.response.status = 400
        ctx.body = e.message
      }finally{
        next()
      }
    },
    async addOrders(ctx, next){
      try{
        const authen = await axios.get(`http://localhost:3100/`, {
          headers: {'Authorization': `Bearer ${ctx.request.token}`}
        })
        if(authen.data.id){
          const result = await orders.addOrder(pool, {...ctx.request.body, users_id: authen.data.id })
          ctx.body = result
        }else{
          ctx.response.status = 400
          ctx.body = 'authen fail'
        }
      }catch(e){
        ctx.response.status = 400
        ctx.body = e.message
      }finally{
        next()
      }
    },
    async removeOrder(ctx, next){
      try{
        const authen = await axios.get(`http://localhost:3100/`, {
          headers: {'Authorization': `Bearer ${ctx.request.token}`}
        })
        if(authen.data.id){
          const { id } = ctx.params
          const result = await orders.removeOrder(pool, parseInt(id))
          ctx.body = result
        }else{
          ctx.response.status = 400
          ctx.body = 'authen fail'
        }
      }catch(e){
        ctx.response.status = 400
        ctx.body = e.message
      }finally{
        next()
      }
    },
  }
}
