const users = require('../models/users')
const config = require('./../config/config.json')
const { getUser } = require('../models/users')

module.exports = function (pool, jwt) {
  return {
    async signIn(ctx, next){
      try{
        const { userName, password } = ctx.request.body
        const result = await users.getUser(pool, { userName, password })
        if(result){
          const payload = {
            id: result.id,
            userName: result.user_name,
            iat: new Date().getTime()
          }
          ctx.status = 200
          ctx.body = {
            token: jwt.sign(payload, config.jwt.secret), //Should be the same secret key as the one used is ./jwt.js
            message: `Successfully logged in!`
          }
        }else{
          ctx.body = `User not found`
        }
      }catch(e){
        ctx.response.status = 400
        ctx.body = e.message
      }finally{
        next()
      }
    },
    async getUser(ctx, next){
      try{
        const decoded = jwt.verify(ctx.request.token, config.jwt.secret);
        if(decoded.id){
          ctx.response.status = 200
          ctx.body = decoded
        }else{
          ctx.response.status = 400
          ctx.body = decoded
        }
      }catch(e){
        ctx.response.status = 400
        ctx.body = e.message
      }finally{
        next()
      }
    }
  }
}
