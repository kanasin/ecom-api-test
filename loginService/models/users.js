module.exports = {
  async getUser(pool, data){
    const query = `select * from users where user_name = ? and password = ?`
    const [rows] = await pool.query(query, [ data.userName, data.password ])
    return rows[0]
  }
}
