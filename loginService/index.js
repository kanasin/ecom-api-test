const Koa = require('koa');
const app = new Koa();
const bodyParser = require("koa-bodyparser");
const Router = require('koa-router')
const router = new Router()
const jwt = require("jsonwebtoken");
const bearerToken = require('koa-bearer-token')
const mysql = require('mysql2/promise')

const users = require('./controllers/users')

const config = require('./config/config.json')

const port = 3100

const pool = mysql.createPool({
  user: config.db.user, 
  password: config.db.password, 
  database: config.db.db, 
  host: config.db.host,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0
})

const usersManage = users(pool, jwt)

router.get("/", usersManage.getUser)

router.post(`/login`, usersManage.signIn)

app.use(bodyParser())
app.use(bearerToken())
app.use(router.routes())

console.log(`Server listening on ${port}`);
app.listen(port);
